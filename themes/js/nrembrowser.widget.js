
/**
 * @file
 * JavaScript for the Node Reference/Embed Media Browser (nrembrowser) widget.
 */

// Add this to the jQuery namespace.
(function ($) {
  // Our 'global' variable for the dialog jQuery object.
  var $nrembrowserDialog;

  /**
   * Replace the textfields with links to the browser.
   */
  Drupal.behaviors.nrembrowserReplaceTextfields = function(context) {
    for (var id in Drupal.settings.nrembrowser) {
      var textfield = '#' + id + '-nid-nid';
      var wrapper = textfield + '-wrapper';

      // Hide the autocomplete textfield.
      $(textfield, context).not('.nrembrowserReplaceTextfields-processed').addClass('nrembrowserReplaceTextfields-processed').hide();

      // Add a link to launch the browser.
      $(wrapper, context).not('.nrembrowserReplaceTextfields-processed').addClass('nrembrowserReplaceTextfields-processed').append(Drupal.settings.nrembrowser[id].launchLink);

      // Launch the browser when clicking the new link.
      $(wrapper + ' .nrembrowser-launch-link', context).not('.nrembrowserReplaceTextfields-processed').addClass('nrembrowserReplaceTextfields-processed').each(function() {
        $(this).attr('id', id + '-nid-nid-nrembrowser-link').bind('click', function() {
          // Launch the browser: @params $link, _current ('title [nid:NID]').
          nrembrowserDialog($(this), $(this).siblings('input').val());
          // Halt further processing.
          return false;
        }).data('field', textfield);
      });

      // Link the thumbnail to that same link.
      $(wrapper, context).siblings('.nrembrowser-thumbnail').children('a:not(.nrembrowserReplaceTextfields-processed)').addClass('nrembrowserReplaceTextfields-processed').each(function() {
        $(this).data('launchLink', $($(wrapper + ' .nrembrowser-launch-link')));
        $(this).bind('click', function() {
          // Trigger the browser link.
          $(this).data('launchLink').click();
          // Halt further processing.
          return false;
        });
      });
    }
  }

  /**
   * Helper function to build the browser.
   *
   * @param $link
   *  The jQuery object for the field link we're launching from.
   * @param _current
   *  The currently selected media node. In a form that may be parsed by
   *  nodereference autocomplete, such as 'Title [nid:XY]' or '[nid:NN]'.
   */
  nrembrowserDialog = function($link, _current) {
    // Create our media browser dialog.
    $nrembrowserDialog = $('<div id="nrembrowser-dialog"></div>')
      .dialog({
        autoopen: false,
        modal: true,
        width: 642,
        title: 'Add media'
      })
      // Remove the dialog entirely from the DOM after closing.
      .bind( "dialogclose", function(event, ui) {
        $(this).remove();
      })
      // Store the passed parameters.
      .data('link', $link).data('current', _current)
      // Store the referenced field to the dialog jquery data.
      .data('field', $link.data('field'))
      // Store the HTML of the original thumbnail.
      .data('currentThumbnail', $link.parent().siblings('.nrembrowser-thumbnail').html());
    // Grab the remote output for the browser.
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: $link.attr('href'),
      data: { js: true, current: _current },
      success: function(data, textStatus, XMLHttpRequest) {
        if (data.status) {
          // Open the browser.
          $nrembrowserDialog.html(data.data).dialog('open');
          // Attach any required behaviors to the browser.
          Drupal.attachBehaviors($nrembrowserDialog);
        }
        else {
          // Failure...
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // Failure...
      }
    });
  }

  // Behaviors added to the basic media browser dialog form elements.
  Drupal.behaviors.nrembrowserPassSelection = function(context) {
    // After hitting OK, we need to change the value of the field.
    $('#nrembrowser-add-media-page-form:not(.nrembrowserPassSelection-processed)', context).addClass('nrembrowserPassSelection-processed').bind('submit', function() {
      // We need the field element to change.
      var _value = '[nid:' + $('#nrembrowser-add-media-page-form input:checked').val() + ']';
      // Also the newly selected thumbnail, if it was changed.
      var _thumbnail = $nrembrowserDialog.data('currentThumbnail');

      // Change the value and thumbnail of the associated field.
      var $field = $($nrembrowserDialog.data('field'));
      $field.val(_value).parent().siblings('.nrembrowser-thumbnail').html(_thumbnail)
      // Ensure we run the correct behaviors again...
      .children('a').removeClass('nrembrowserReplaceTextfields-processed');
      $field.removeClass('nrembrowserReplaceTextfields-processed');

      // Run the behaviors on the new thumbnail.
      Drupal.attachBehaviors($field.parent().parent());

      // Close the dialog.
      $nrembrowserDialog.dialog('close');

      // Halt further processing.
      return false;
    });

    // Bind clicking the thumbnail to selecting the corresponding radio.
    $('#nrembrowser-add .nrembrowser-thumbnail:not(.nrembrowserPassSelection-processed)', context).addClass('nrembrowserPassSelection-processed').each(function() {
      $(this).bind('click', function() {
        // Select the associated radio when clicking the thumbnail.
        $(this).siblings('label').children('input').attr('checked', 1).trigger('change').focus();

        // Draw a rectangle around the selected thumbnail.
        $('#nrembrowser-add .nrembrowser-thumbnail').removeClass('selected');
        $(this).addClass('selected');

        // Halt further processing.
        return false;
      }).bind('dblclick', function() {
        // Double-clicking a thumbnail submits the form.
        $('#nrembrowser-add-media-page-form').submit();
      }).bind('keypress', function(event) {
        // Pressing enter after selecting a thumbnail submits the form.
        if (event.which == 13) {
          $('#nrembrowser-add-media-page-form').submit();
          // Halt further processing.
          return false;
        }
      });
    });

    // Change the selected thumbnail after making a new radio selection.
    $('#nrembrowser-add-media-page-form input[name=media]', context).not('.nrembrowserPassSelection-processed').addClass('nrembrowserPassSelection-processed').each(function() {
      $(this).bind('change', function() {
        $nrembrowserDialog.data('currentThumbnail', $(this).parent().siblings('.nrembrowser-thumbnail').html());
      }).hide();
    });
  }

})(jQuery);
